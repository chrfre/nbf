# NBF Nested Blocks Format

Licensed under the Mozilla Public License, version 2.0.

## About

This crate provides a draft implementation of a parser for the "Nested Blocks Format" (short NBF).
NBF is a text file format, which is specifically designed for humans to be able to express nested or hierarchical structures in the form of blocks.
For details check out the [documentation](https://docs.rs/nbf).
