/*
Copyright (c) 2023 Christoph Freitag

Licensed under the Mozilla Public License, version 2.0.
*/

//! This crate contains a draft implementation of the Nested Blocks Format (NBF),
//! which is a human friendly format that allows expressing nested or
//! hierarchical data.
//! The basic unit in NBF is a block, containing zero or any number of nested
//! children:
//! ```txt
//! block type: name of first block {
//!     another block type: name of nested block {
//!     }
//! }
//! block type: name of second block
//! ```
//! In this example `block type` is the *type* of the two outer blocks, also
//! referred to as the block's *key word(s)*.
//! `name of the first block` is the first block's *header value*.
//! The first outer block has a singe *child*, whereas the second block has no
//! *children*.
//! (Brakets may be omitted whenever a block has no children.)
//! 
//! For a real example, a dataset in NBF might look like:
//! ```txt
//! person: Albert Einstein {
//!     year of birth: 1879
//! }
//! person: Max Planck {
//!     year of birth: 1858
//! }
//! ```
//! The two given blocks which define persons contain the inner blocks (without
//! brakets)
//! ```txt
//! year of birth: 1879
//! ```
//! and
//! ```txt
//! year of birth: 1858
//! ```
//!
//! There is no limit to the nesting, except for the limits which the user
//! imposes in his or her implementation.
//! For instance, a `person` could also have one or multiple address blocks assigned:
//! ```txt
//! person: Richard Feynman {
//!     year of birth: 1918
//!     address: home {
//!         street name: ...
//!         street number: ...
//!         city/town/village: ...
//!         county/state: ...
//!     }
//!     address: work {
//!         street name: ...
//!         street number: ...
//!         city/town/village: ...
//!         county/state: ...
//!     }
//! }
//! ```
//!
//! For details of the implementation see the [`parser`] module.

pub mod parser;

